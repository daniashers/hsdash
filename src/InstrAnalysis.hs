module InstrAnalysis where

import           CoreTypes
import           Data.Maybe
import           Instructions
import           ShowUtil

data JumpTarget = Known Addr | Unknown deriving Eq

instance Show JumpTarget where
  show (Known addr) = "Known " ++ showhex4 addr
  show Unknown      = "Unknown"

knownTargets :: [JumpTarget] -> [Addr]
knownTargets =
  mapMaybe (\tgt -> case tgt of
    Known(a) -> Just a
    Unknown  -> Nothing
  )


nextAddr :: TaggedInstr -> Addr
nextAddr (TaggedInstr addr i) = fromIntegral (fromIntegral addr + instrLen i)

jumpTargets :: TaggedInstr -> [JumpTarget]
jumpTargets ti@(TaggedInstr addr i) = case i of
  BCC (RelativeJ rel) -> [Known (next +- rel), Known next]
  BCS (RelativeJ rel) -> [Known (next +- rel), Known next]
  BEQ (RelativeJ rel) -> [Known (next +- rel), Known next]
  BMI (RelativeJ rel) -> [Known (next +- rel), Known next]
  BNE (RelativeJ rel) -> [Known (next +- rel), Known next]
  BPL (RelativeJ rel) -> [Known (next +- rel), Known next]
  BVC (RelativeJ rel) -> [Known (next +- rel), Known next]
  BVS (RelativeJ rel) -> [Known (next +- rel), Known next]
  JMP (AbsoluteJ abs) -> [Known (abs)]
  JMP (IndirectJ ind) -> [Unknown]
  JSR (AbsoluteJ abs) -> [Known abs]
  RTI                 -> [Unknown]
  RTS                 -> [Unknown]
  BRK                 -> [Unknown]
  _                   -> []
  where next = nextAddr ti

addressesToExplore :: TaggedInstr -> [Addr]
addressesToExplore ti@(TaggedInstr _ (JSR (AbsoluteJ abs))) = [abs, nextAddr ti] -- special case for subroutine call: explore the next instruction as well
addressesToExplore ti = knownTargets $ jumpTargets ti

-- Check if there is a possibility that the instrucion may perform a jump. (includes conditional and unconditional)
mayJump :: Instr -> Bool
mayJump instr = not $ null $ jumpTargets (TaggedInstr 0 instr)

mayNotJump :: Instr -> Bool
mayNotJump = not . willJump

-- Identifies the instructions which perform an unconditional jump.
willJump :: Instr -> Bool
willJump (JMP (AbsoluteJ abs)) = True
willJump (JMP (IndirectJ ind)) = True
willJump (JSR (AbsoluteJ abs)) = True
willJump RTI                   = True
willJump RTS                   = True
willJump BRK                   = True
willJump _                     = False

absolutiseInstr :: TaggedInstr -> Instr
absolutiseInstr (TaggedInstr addr i@(BEQ (RelativeJ rel))) = BEQ (AbsoluteJ (addr +- (instrLen i) +- rel))
absolutiseInstr (TaggedInstr addr i@(BNE (RelativeJ rel))) = BNE (AbsoluteJ (addr +- (instrLen i) +- rel))
absolutiseInstr (TaggedInstr addr i@(BCS (RelativeJ rel))) = BCS (AbsoluteJ (addr +- (instrLen i) +- rel))
absolutiseInstr (TaggedInstr addr i@(BCC (RelativeJ rel))) = BCC (AbsoluteJ (addr +- (instrLen i) +- rel))
absolutiseInstr (TaggedInstr addr i@(BMI (RelativeJ rel))) = BMI (AbsoluteJ (addr +- (instrLen i) +- rel))
absolutiseInstr (TaggedInstr addr i@(BPL (RelativeJ rel))) = BPL (AbsoluteJ (addr +- (instrLen i) +- rel))
absolutiseInstr (TaggedInstr addr i@(BVS (RelativeJ rel))) = BVS (AbsoluteJ (addr +- (instrLen i) +- rel))
absolutiseInstr (TaggedInstr addr i@(BVC (RelativeJ rel))) = BVC (AbsoluteJ (addr +- (instrLen i) +- rel))
absolutiseInstr (TaggedInstr _    anyOtherInstr)           = anyOtherInstr
