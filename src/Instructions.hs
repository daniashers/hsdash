module Instructions where

import           Data.Bits
import           Data.Int
import           Data.Word
import           Text.Printf

data AddrMode = Accumulator
              | Immediate Word8
              | ZeroPage Word8
              | ZeroPageIndexedX Word8
              | ZeroPageIndexedY Word8
              | Absolute Word16
              | AbsoluteIndexedX Word16
              | AbsoluteIndexedY Word16
              | IndexedIndirectX Word8
              | IndirectIndexedY Word8
              deriving Eq

instance Show AddrMode where
    show Accumulator           = "A"
    show (Immediate d)         = printf "#$%02X" d
    show (ZeroPage az)         = printf "$%02X" az
    show (ZeroPageIndexedX az) = printf "$%02X, X" az
    show (ZeroPageIndexedY az) = printf "$%02X, Y" az
    show (Absolute a)          = printf "$%04X" a
    show (AbsoluteIndexedX a)  = printf "$%04X, X" a
    show (AbsoluteIndexedY a)  = printf "$%04X, Y" a
    show (IndexedIndirectX az) = printf "($%02X, X)" az
    show (IndirectIndexedY az) = printf "($%02X), X" az

data JumpMode = AbsoluteJ Word16
              | IndirectJ Word16
              | RelativeJ Int8
              deriving Eq

instance Show JumpMode where
    show (AbsoluteJ a)   = printf "$%04X" a
    show (IndirectJ i)   = printf "($%04X)" i
    show (RelativeJ rel) = printf "$%+02X" rel

-- jumps:
--
-- BCC
-- BCS
-- BEQ
-- BMI
-- BNE
-- BPL
-- BRK
-- BVC
-- BVS
-- JMP
-- JSR
-- PHA ?
-- PHP ?
-- PLA ?
-- PLP ?
-- RTI
-- RTS

data Instr = ADC AddrMode
           | AND AddrMode
           | ASL AddrMode
           | BCC JumpMode
           | BCS JumpMode
           | BEQ JumpMode
           | BIT AddrMode
           | BMI JumpMode
           | BNE JumpMode
           | BPL JumpMode
           | BRK
           | BVC JumpMode
           | BVS JumpMode
           | CLC
           | CLD
           | CLI
           | CLV
           | CMP AddrMode
           | CPX AddrMode
           | CPY AddrMode
           | DEC AddrMode
           | DEX
           | DEY
           | EOR AddrMode
           | INC AddrMode
           | INX
           | INY
           | JMP JumpMode
           | JSR JumpMode
           | LDA AddrMode
           | LDX AddrMode
           | LDY AddrMode
           | LSR AddrMode
           | NOP
           | ORA AddrMode
           | PHA
           | PHP
           | PLA
           | PLP
           | ROL AddrMode
           | ROR AddrMode
           | RTI
           | RTS
           | SBC AddrMode
           | SEC
           | SED
           | SEI
           | STA AddrMode
           | STX AddrMode
           | STY AddrMode
           | TAX
           | TAY
           | TSX
           | TXA
           | TXS
           | TYA
           | BAD -- placeholder for illegal instruction
           deriving (Show, Eq)

word :: Word8 -> Word8 -> Word16
word h l = ((word .|. (fromIntegral h)) `shiftL` 8) .|. (fromIntegral l)
    where
        word :: Word16
        word = 0

toSigned :: Word8 -> Int8
toSigned x = fromIntegral x

instrLen :: Instr -> Int
instrLen instr = case instr of
    BRK                      -> 1
    PHP                      -> 1
    ASL Accumulator          -> 1
    CLC                      -> 1
    PLP                      -> 1
    ROL Accumulator          -> 1
    SEC                      -> 1
    RTI                      -> 1
    PHA                      -> 1
    LSR Accumulator          -> 1
    CLI                      -> 1
    RTS                      -> 1
    PLA                      -> 1
    ROR Accumulator          -> 1
    SEI                      -> 1
    DEY                      -> 1
    TXA                      -> 1
    TYA                      -> 1
    TXS                      -> 1
    TAY                      -> 1
    TAX                      -> 1
    CLV                      -> 1
    TSX                      -> 1
    INY                      -> 1
    DEX                      -> 1
    CLD                      -> 1
    INX                      -> 1
    NOP                      -> 1
    SED                      -> 1
    ORA (IndexedIndirectX _) -> 2
    ORA (ZeroPage _)         -> 2
    ASL (ZeroPage _)         -> 2
    ORA (Immediate _)        -> 2
    BPL (RelativeJ _)        -> 2
    ORA (IndirectIndexedY _) -> 2
    ORA (ZeroPageIndexedX _) -> 2
    ASL (ZeroPageIndexedX _) -> 2
    AND (IndexedIndirectX _) -> 2
    BIT (ZeroPage _)         -> 2
    AND (ZeroPage _)         -> 2
    ROL (ZeroPage _)         -> 2
    AND (Immediate _)        -> 2
    BMI (RelativeJ _)        -> 2
    AND (IndirectIndexedY _) -> 2
    AND (ZeroPageIndexedX _) -> 2
    ROL (ZeroPageIndexedX _) -> 2
    EOR (IndexedIndirectX _) -> 2
    EOR (ZeroPage _)         -> 2
    LSR (ZeroPage _)         -> 2
    EOR (Immediate _)        -> 2
    BVC (RelativeJ _)        -> 2
    EOR (IndirectIndexedY _) -> 2
    EOR (ZeroPageIndexedX _) -> 2
    LSR (ZeroPageIndexedX _) -> 2
    ADC (IndexedIndirectX _) -> 2
    ADC (ZeroPage _)         -> 2
    ROR (ZeroPage _)         -> 2
    ADC (Immediate _)        -> 2
    BVS (RelativeJ _)        -> 2
    ADC (IndirectIndexedY _) -> 2
    ADC (ZeroPageIndexedX _) -> 2
    ROR (ZeroPageIndexedX _) -> 2
    STA (IndexedIndirectX _) -> 2
    STY (ZeroPage _)         -> 2
    STA (ZeroPage _)         -> 2
    STX (ZeroPage _)         -> 2
    BCC (RelativeJ _)        -> 2
    STA (IndirectIndexedY _) -> 2
    STY (ZeroPageIndexedX _) -> 2
    STA (ZeroPageIndexedX _) -> 2
    STX (ZeroPageIndexedY _) -> 2
    LDY (Immediate _)        -> 2
    LDA (IndexedIndirectX _) -> 2
    LDX (Immediate _)        -> 2
    LDY (ZeroPage _)         -> 2
    LDA (ZeroPage _)         -> 2
    LDX (ZeroPage _)         -> 2
    LDA (Immediate _)        -> 2
    BCS (RelativeJ _)        -> 2
    LDA (IndirectIndexedY _) -> 2
    LDY (ZeroPageIndexedX _) -> 2
    LDA (ZeroPageIndexedX _) -> 2
    LDX (ZeroPageIndexedY _) -> 2
    CPY (Immediate _)        -> 2
    CMP (IndexedIndirectX _) -> 2
    CPY (ZeroPage _)         -> 2
    CMP (ZeroPage _)         -> 2
    DEC (ZeroPage _)         -> 2
    CMP (Immediate _)        -> 2
    BNE (RelativeJ _)        -> 2
    CMP (IndirectIndexedY _) -> 2
    CMP (ZeroPageIndexedX _) -> 2
    DEC (ZeroPageIndexedX _) -> 2
    CPX (Immediate _)        -> 2
    SBC (IndexedIndirectX _) -> 2
    CPX (ZeroPage _)         -> 2
    SBC (ZeroPage _)         -> 2
    INC (ZeroPage _)         -> 2
    SBC (Immediate _)        -> 2
    BEQ (RelativeJ _)        -> 2
    SBC (IndirectIndexedY _) -> 2
    SBC (ZeroPageIndexedX _) -> 2
    INC (ZeroPageIndexedX _) -> 2
    ORA (Absolute _)         -> 3
    ASL (Absolute _)         -> 3
    ORA (AbsoluteIndexedY _) -> 3
    ORA (AbsoluteIndexedX _) -> 3
    ASL (AbsoluteIndexedX _) -> 3
    JSR (AbsoluteJ _)        -> 3
    BIT (Absolute _)         -> 3
    AND (Absolute _)         -> 3
    ROL (Absolute _)         -> 3
    AND (AbsoluteIndexedY _) -> 3
    AND (AbsoluteIndexedX _) -> 3
    ROL (AbsoluteIndexedX _) -> 3
    JMP (AbsoluteJ _)        -> 3
    EOR (Absolute _)         -> 3
    LSR (Absolute _)         -> 3
    EOR (AbsoluteIndexedY _) -> 3
    EOR (AbsoluteIndexedX _) -> 3
    LSR (AbsoluteIndexedX _) -> 3
    JMP (IndirectJ _)        -> 3
    ADC (Absolute _)         -> 3
    ROR (Absolute _)         -> 3
    ADC (AbsoluteIndexedY _) -> 3
    ADC (AbsoluteIndexedX _) -> 3
    ROR (AbsoluteIndexedX _) -> 3
    STY (Absolute _)         -> 3
    STA (Absolute _)         -> 3
    STX (Absolute _)         -> 3
    STA (AbsoluteIndexedY _) -> 3
    STA (AbsoluteIndexedX _) -> 3
    LDY (Absolute _)         -> 3
    LDA (Absolute _)         -> 3
    LDX (Absolute _)         -> 3
    LDA (AbsoluteIndexedY _) -> 3
    LDY (AbsoluteIndexedX _) -> 3
    LDA (AbsoluteIndexedX _) -> 3
    LDX (AbsoluteIndexedY _) -> 3
    CPY (Absolute _)         -> 3
    CMP (Absolute _)         -> 3
    DEC (Absolute _)         -> 3
    CMP (AbsoluteIndexedY _) -> 3
    CMP (AbsoluteIndexedX _) -> 3
    DEC (AbsoluteIndexedX _) -> 3
    CPX (Absolute _)         -> 3
    SBC (Absolute _)         -> 3
    INC (Absolute _)         -> 3
    SBC (AbsoluteIndexedY _) -> 3
    SBC (AbsoluteIndexedX _) -> 3
    INC (AbsoluteIndexedX _) -> 3
