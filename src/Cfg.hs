module Cfg where

import           CodeBlock
import           CoreTypes
import           Data.Graph.Inductive
import           Data.Graph.Inductive.Graph
import           Data.List
import           InstrAnalysis
import           Instructions
import           ShowUtil

-- Create a control flow graph from the given set of code blocks.
mkCfg :: [CodeBlock] -> Gr CodeBlock ()
mkCfg blocks =
  let
    nodes :: [LNode CodeBlock]
    nodes = map (\cb -> (fromIntegral $ startAddr cb, cb)) blocks
    edges :: [LEdge ()]
    edges = blocks >>= edgesFromBlock
  in mkGraph nodes edges

-- A block is well-formed only if the only possible jumps are in the last instruction.
-- Put it another way, if we remove the last instruction there should be no jumps.
isBlockWellFormed :: CodeBlock -> Bool
isBlockWellFormed cb@(CodeBlock addr instrs) =
  null $ jumpTargetsOfBlock (CodeBlock addr (init instrs))

edgesFromBlock :: CodeBlock -> [LEdge ()]
edgesFromBlock cb@(CodeBlock origin instrs) =
  if (not $ isBlockWellFormed cb)
    then error $ "The block at " ++ (showhex4 origin) ++ "is malformed!"
    else case lastInstr of
      TaggedInstr addr (JSR (AbsoluteJ tgt)) -> [(fromIntegral origin, fromIntegral tgt, ()), (fromIntegral tgt, fromIntegral (nextAddr lastInstr), ())]
      TaggedInstr addr instr -> nub $ (map (\tgt -> (fromIntegral origin, fromIntegral tgt, ())) (knownTargets $ jumpTargets lastInstr)) ++ (if (mayNotJump instr) then [(fromIntegral origin, fromIntegral $ nextAddr lastInstr, ())] else [])
  where lastInstr = last $ labelInstructions cb
