module BasicBlock where

import CodeBlock

data BasicBlock = BasicBlock {
  instructions :: [Instr]
  targets :: [Addr]
} deriving Show

mkBasicBlock :: CodeBlock -> Maybe BasicBlock
mkBasicBlock (CodeBlock start instrs) = undefined
  let
    reducedBlock = CodeBlock start tail instrs
  in expression
