module CoreTypes where

import           Data.Word
import           Instructions
import           Text.Printf

type Addr = Word16

data TaggedInstr = TaggedInstr Addr Instr

instance Show TaggedInstr where
    show (TaggedInstr addr instr) = (printf "%04X" addr) ++ " " ++ (show instr)

-- Utility function to add an address and a relative jump without type conversion clutter
(+-) :: Integral a => Addr -> a -> Addr
source +- delta = (fromIntegral (((fromIntegral source) :: Int) + ((fromIntegral delta) :: Int)))
