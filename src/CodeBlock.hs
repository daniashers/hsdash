module CodeBlock where

import           CoreTypes
import           Data.Int
import           Data.List
import           Data.Maybe
import           InstrAnalysis
import           Instructions
import           ShowUtil

data CodeBlock = CodeBlock Addr [Instr] deriving (Show, Eq)

startAddr :: CodeBlock -> Addr
startAddr (CodeBlock addr _) = addr

labelInstructions :: CodeBlock -> [TaggedInstr]
labelInstructions (CodeBlock _ []) = []
labelInstructions (CodeBlock base (h:t)) = (TaggedInstr base h) : (labelInstructions (CodeBlock (base + fromIntegral (instrLen h)) t))

blockEndAddr :: CodeBlock -> Addr
blockEndAddr (CodeBlock base is) = base + (fromIntegral totalLength)
                                   where totalLength = (sum . map instrLen) is
prettyPrintBlock :: CodeBlock -> String
prettyPrintBlock cb = (intercalate "\n" . (map show) . labelInstructions . absolutiseBranches) cb

-- This function assumes that the block is "well formed", i.e. the only outward jumps are in the last instruction!
-- TODO this is not always the case. Sometimes blocks are split at non-jumps by discovering jump targets there.
jumpTargetsOfBlock :: CodeBlock -> [JumpTarget]
jumpTargetsOfBlock cb = (nub . concat . (map jumpTargets) . labelInstructions) cb

addressesToExploreForBlock :: CodeBlock -> [Addr]
addressesToExploreForBlock cb@(CodeBlock base is) =
  (labelInstructions cb) >>= addressesToExplore

-- Answers whether a given block includes (or starts with) a given address.
includes :: Addr -> CodeBlock -> Bool
includes addr cb@(CodeBlock base is) = (addr >= base) && (addr < blockEndAddr cb)

-- Answers whether a block includes a given address AND there is an actual instruction starting at that address
includesProperly :: CodeBlock -> Addr -> Bool
includesProperly cb addr = (includes addr cb) && (((elem addr) . (map (\(TaggedInstr a i) -> a)) . labelInstructions) cb)

splitBlock :: CodeBlock -> Addr -> [CodeBlock]
splitBlock cb@(CodeBlock addr is) splitpoint
  | splitpoint == addr = [cb] -- if the splitpoint is the beginning of the block, do nothing and return the block whole!
  | includes splitpoint cb = let
      instrAddrs = ((map (\(TaggedInstr a i) -> a)) . labelInstructions) cb
      in case elemIndex splitpoint instrAddrs of
        Just j  -> let (block1, block2) = splitAt j is
                   in [CodeBlock addr block1, CodeBlock splitpoint block2]
        Nothing -> error "NOOOOOOOO!" -- most likely there is no instruction starting at that address!
  | otherwise = [cb] -- if the splitpoint is not part of the block, do nothing and return the original block whole!

truncateAt :: Addr -> CodeBlock -> CodeBlock
truncateAt addr cb = head (splitBlock cb addr)

absolutiseBranches :: CodeBlock -> CodeBlock
absolutiseBranches cb@(CodeBlock base instrs) =
    let
        taggedInstrs = labelInstructions cb
    in (CodeBlock base (map absolutiseInstr taggedInstrs))
