module ShowUtil where

import Text.Printf
import Data.Word

showhex4 :: Word16 -> String
showhex4 x = printf "%04X" x