module EasyDecoder where

import           CodeBlock
import           CoreTypes
import           Data.Bits
import           Data.List
import           Data.Word
import           Debug.Trace
import           InstrAnalysis
import           InstructionDecoder
import           Instructions
import           ShowUtil
import           Text.Printf

decodeBlock :: Addr -> [Word8] -> (CodeBlock, Int)
decodeBlock base bs = let go :: [Instr] -> [Word8] -> Int -> ([Instr], Int)
                          go accum [] n = (accum, n)
                          go accum bs n = case (decode bs) of
                                              (BAD, _)                -> (accum, n)
                                              (newInstr, newConsumed) -> go (accum ++ [newInstr]) (drop newConsumed bs) (n + newConsumed)
                          (is, n) = go [] bs 0
                      in (CodeBlock base is, n)

data DecodeStatus = Continue | EndInclusive | EndExclusive

-- decode a code block starting at the given address, until a specified condition is encountered
decodeBlockWhile :: Addr -> (TaggedInstr -> DecodeStatus) -> [Word8] -> CodeBlock
decodeBlockWhile base f bytes =
  let
    go :: [Instr] -> [Word8] -> Int -> ([Instr], Int)
    go accum [] n = (accum, n)
    go accum bytes n =
      let
        (decoded, consumed) = (decode bytes)
      in case f (TaggedInstr (base + (fromIntegral n)) decoded) of
        Continue     -> go (accum ++ [decoded]) (drop consumed bytes) (n + consumed)
        EndInclusive -> (accum ++ [decoded], n + consumed)
        EndExclusive -> (accum, n)
    (is, n) = go [] bytes 0
  in CodeBlock base is

myBlockDecode :: Addr      -- the base address to start decoding from
              -> [Addr]    -- a 'kill list' of addresses. If we run into one of these, end decoding immediately
              -> [Word8]   -- the binary code to decode
              -> CodeBlock
myBlockDecode base stopAddrs = decodeBlockWhile base f
  where
    f (TaggedInstr addr instr)
      | instr == BAD        = EndExclusive
      | elem addr stopAddrs = EndExclusive
      | mayJump instr       = EndInclusive
      | otherwise           = Continue

exploringDecode :: Addr -> [Word8] -> [CodeBlock]
exploringDecode base bytes =
    let
        go :: [CodeBlock] -> Addr -> [CodeBlock]
        go existingBlocks addr =
            let
                byteOffset = fromIntegral addr - fromIntegral base
                existingStartAddresses = map startAddr existingBlocks
                newBlock = myBlockDecode addr existingStartAddresses (drop byteOffset bytes)
                newExistingBlocks = existingBlocks ++ [newBlock]
                -- now filter out any jumps to already explored blocks
                targetAddrsToExplore = filter (\a -> notElem a (map startAddr newExistingBlocks)) (addressesToExploreForBlock newBlock)
                aggreg :: [CodeBlock] -> Addr -> [CodeBlock]
                aggreg cbz jt = case find (includes jt) cbz of -- is there a block that already includes the jump target?
                    Nothing -> go cbz jt -- No: decode a new block starting there.
                    Just cb@(CodeBlock addr is)
                        | jt == addr -> cbz -- Yes, it starts at the address: don't explore further
                        | otherwise  -> (filter (\b -> (startAddr b) /= addr) cbz) ++ (splitBlock cb jt) -- Yes but it falls in the middle of an existing block: split it in two
            in trace (show $ map showhex4 targetAddrsToExplore) (foldl aggreg newExistingBlocks targetAddrsToExplore)
    in go [] base
