import           Test.Hspec

import           CodeBlock
import           CoreTypes
import           EasyDecoder
import           Instructions

main :: IO ()
main = hspec (spec >> spec2)

spec :: Spec
spec = do
  describe "CodeBlock" $ do
    it "split a block correctly" $ do
      (splitBlock wholeBlock 2) `shouldBe` [halfBlock1, halfBlock2]
          where
              wholeBlock = CodeBlock 0 [LDA (Immediate 0x0f), STA (Absolute 0xd418)]
              halfBlock1 = CodeBlock 0 [LDA (Immediate 0x0f)]
              halfBlock2 = CodeBlock 2 [STA (Absolute 0xd418)]

spec2 :: Spec
spec2 = describe "Branch absolutisation" $ do
  it "should absolutise a branch correctly" $ do
    (absolutiseInstr $ TaggedInstr 0x8000 (BEQ (RelativeJ 5))) `shouldBe` (BEQ (AbsoluteJ 0x8007))
  it "should leave other instructions unchanged" $ do
    (absolutiseInstr $ TaggedInstr 0x8000 RTS) `shouldBe` (RTS)
