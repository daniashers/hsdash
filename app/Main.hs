module Main where

import           Cfg
import           CodeBlock
import qualified Data.GraphViz  as GV
import           Data.List
import           Data.Ord
import qualified Data.Text.Lazy as TXT
import           EasyDecoder
import           Instructions
import           TestProgram

program = [0xad, 0x20, 0xd0, 0xea, 0xa9, 0x13]

main :: IO ()
main = do
  (Prelude.putStrLn . concat . map (++ "\n\n") . (map prettyPrintBlock) . (sortBy $ comparing startAddr) . (exploringDecode testProgramBaseAddress)) testProgram
  (Prelude.putStrLn . TXT.unpack . GV.printDotGraph . (GV.graphToDot fileGraphParams) . mkCfg . (exploringDecode testProgramBaseAddress)) testProgram
  -- (Prelude.putStrLn . concat . map (++ "\n\n") . (map prettyPrintBlock) . (sortBy $ comparing startAddr) . (exploringDecode shortTestProgramBaseAddress)) shortTestProgram
  -- (Prelude.putStrLn . TXT.unpack . GV.printDotGraph . (GV.graphToDot fileGraphParams) . mkCfg . (exploringDecode shortTestProgramBaseAddress)) shortTestProgram


fileGraphParams :: GV.GraphvizParams Int CodeBlock () () CodeBlock
fileGraphParams = GV.defaultParams
